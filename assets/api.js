let rootURL = 'http://www.omdbapi.com'

exports.search = (movieName, page) => {
    let url = `${rootURL}?apikey=1a21028c&s=${movieName}&page=${page}`
    return fetch(url)
    .then(response => response.json())
    .then(data => {
        return data.Search
    })
}

let likeURL = 'https://aqueous-fortress-96599.herokuapp.com/api/likes'

exports.getLikes = (userId) => {
    let tempURL = likeURL.concat(`?user_id=${userId}`)
    return fetch(tempURL)
    .then(response => {
        return response.json()
    })
    .then(response => {
        return response.data
    })
    .catch( err => console.log(err))
}

exports.postLikes = (userId, imdbId) => {
    return fetch(likeURL, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            user_id: userId,
            imdb_id: imdbId
        })
    })
    .then(response => {return response.json()})
    .then(responseJson => console.log(responseJson))
    .catch(err => console.log(err))
}

exports.removeLikes = (userId, imdbId) => {
    let tempURL = likeURL.concat('/0')
    return fetch(tempURL, {
        method: 'DELETE',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            user_id: userId,
            imdb_id: imdbId
        })
    })
    .then(response => {return response.json()})
    .then(responseJson => console.log(responseJson))
    .catch(err => console.log(err))
}