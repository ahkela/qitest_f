import React, {Component} from 'react'
import {View, Image} from 'react-native'
import { Text, Card, CardItem, Button, Icon, Left, Body, Right } from 'native-base';
import MaterialCommunityIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import api from '../../assets/api'
import firebase from 'firebase'

export default class MovieCard extends Component {
    constructor(props){
        super(props)
        this.state={
            liked: this.props.liked,
            imdbId: this.props.imdbId
        }
    }

    handlePress = () => {
        this.setState({
            liked: !this.state.liked
        }, function(){
            this.postLikes()
        })
    }

    postLikes = async () => {
        let uid = firebase.auth().currentUser.uid
        this.state.liked ? api.postLikes(uid, this.state.imdbId) : api.removeLikes(uid, this.state.imdbId)
        .catch(error => {
            console.log(error);
        })
    }

    render(){
        return(
            <View>
                <Card>
                    <CardItem cardBody>
                        <Image source={{uri: this.props.poster}} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                    <CardItem>
                        <Left>
                            <Body>
                                <Text>{this.props.title}</Text>
                                <Text note>{this.props.year}</Text>
                            </Body>
                        </Left>
                        <Right>
                            <Button transparent
                                onPress={() => {
                                    this.handlePress()
                                }}
                            >
                                <MaterialCommunityIcon 
                                    name={this.state.liked ? "heart" : "heart-outline"}
                                    color={"#E9446A"}
                                    size={20}
                                />
                            </Button>
                        </Right>
                    </CardItem>
                </Card>
            </View>
        )
    }
}