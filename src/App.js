import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {NavigationContainer} from '@react-navigation/native'
import {createStackNavigator} from '@react-navigation/stack'
import LoginScreen from './screens/LoginScreen'
import HomeScreen from './screens/HomeScreen'
import LoadingScreen from './screens/LoadingScreen'
import RegisterScreen from './screens/RegisterScreen'
import {TouchableOpacity, Text, StyleSheet} from 'react-native'

import * as firebase from 'firebase'

var firebaseConfig = {
  apiKey: "AIzaSyBtHPhRIPV8IGLt3AY_c9rZEAQJ4eZwJKY",
  authDomain: "qitest-deb0d.firebaseapp.com",
  databaseURL: "https://qitest-deb0d.firebaseio.com",
  projectId: "qitest-deb0d",
  storageBucket: "qitest-deb0d.appspot.com",
  messagingSenderId: "254114422973",
  appId: "1:254114422973:web:f33619137e17edf876d7d4"
};

firebase.initializeApp(firebaseConfig);

const Stack = createStackNavigator()

console.disableYellowBox = true

export default class App extends Component{
  signOutUser = async () => {
    try {
        await firebase.auth().signOut();
        navigate('Login');
    } catch (e) {
        console.log(e);
    }
  }

  render(){
    return(
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Loading">
          <Stack.Screen name="Login" component={LoginScreen}/>
          {/* <Stack.Screen name="Register" component={RegisterScreen}/> */}
          <Stack.Screen name="Loading" component={LoadingScreen}/>
          <Stack.Screen name="Home" component={HomeScreen}
            options={{
              title: "Movie List",
              headerLeft: () => {},
              headerRight: () => (
                <TouchableOpacity
                  onPress={this.signOutUser}
                >
                  <Text style={styles.logout}>
                    Log out 
                  </Text>
                </TouchableOpacity>
              )
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    )
  }
}

const styles = StyleSheet.create({
  logout: {
    fontWeight: "800", 
    color: "#E9446A",
    paddingHorizontal: 10
  }
})