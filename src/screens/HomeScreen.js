import React, {PureComponent} from 'react';
import {View, StyleSheet, Image, FlatList, TextInput} from 'react-native'
import { Text, Card, CardItem, Button, Icon, Left, Body, Right } from 'native-base';
import api from '../../assets/api'
import firebase from 'firebase'
import MovieCard from '../components/MovieCard'

export default class HomeScreen extends PureComponent {
    constructor(props){
        super(props)
        this.state = {
            data: [],
            movieName: "",
            page: 1,
            likes: []
        }
    }

    componentDidMount(){
        let userId = firebase.auth().currentUser.uid
        api.getLikes(userId)
        .then(response=> {
            let temp = []
            console.log('response', response)
            if(response.length > 0){
                for(let ele of response){
                    console.log(ele)
                    temp.push(ele.imdbId)
                }
                this.setState({
                    likes: temp
                }, function(){
                    console.log(this.state.likes)
                })
            }
        })
        .catch(err => console.log(err))
    }

    renderRow = ({item}) => {
        console.log(item.imdbID)
        return (
            <MovieCard
                liked={this.state.likes.includes(item.imdbID)}
                imdbId={item.imdbID}
                poster={item.Poster}
                title={item.Title}
                year={item.Year}
            />
        )
    }

    search = async () => {
        api.search(this.state.movieName, this.state.page)
        .then((movies) => {
            if(movies != undefined) {
                this.setState({
                    data: this.state.data.concat(movies)
                },  this.getLikes)
            }
        })
        .catch(error => {
            
        })
    }

    render() {
        return (
            <View>
                <TextInput
                    placeholder={"Enter a movie..."}
                    style={styles.searchbox}
                    value={this.state.movieName}
                    onChangeText={(movieName) => {
                        this.setState({
                            movieName: movieName,
                            page: 1
                        })
                    }}
                    onEndEditing={() => this.search()}
                />
                <FlatList
                    data={this.state.data}
                    renderItem={this.renderRow}
                    keyExtractor={(item, index) => String(index)}
                    onEndReached={() => {
                        this.setState({
                            page: this.state.page + 1
                        }, this.search)
                    }}
                    contentContainerStyle={styles.list}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    searchbox: {
        fontSize: 20,
        fontWeight: '300',
        padding: 20,
        width: '98%',
        backgroundColor: '#FFF',
        borderRadius: 4,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: 'center'
    },
    list: {
        paddingBottom: 100
    }
})